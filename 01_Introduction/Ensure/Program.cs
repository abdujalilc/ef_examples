﻿using (ApplicationContext db = new ApplicationContext())
{
    var test1 = db.Database.EnsureDeleted();
    var test2 = db.Database.EnsureCreated();
    // асинхронная версия
    //await db.Database.EnsureCreatedAsync();
    //await db.Database.EnsureDeletedAsync();

    bool isAvalaible = db.Database.CanConnect();
    bool isAvalaible2 = await db.Database.CanConnectAsync();
    if (isAvalaible)
        Console.WriteLine("База данных доступна");
    else
        Console.WriteLine("База данных не доступна");
}