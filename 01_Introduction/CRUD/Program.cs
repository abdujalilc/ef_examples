﻿using Microsoft.EntityFrameworkCore;    // для ToListAsync и FirstOrDefaultAsync
// Добавление
using (ApplicationContext db = new ApplicationContext())
{
    User tom = new User { Name = "Tom", Age = 33 };
    User alice = new User { Name = "Alice", Age = 26 };

    // Добавление
    await db.Users.AddRangeAsync(tom, alice);
    await db.SaveChangesAsync();
}

// получение
using (ApplicationContext db = new ApplicationContext())
{
    // получаем объекты из бд и выводим на консоль
    var users = await db.Users.ToListAsync();
    Console.WriteLine("Данные после добавления:");
    foreach (User u in users)
    {
        Console.WriteLine($"{u.Id}.{u.Name} - {u.Age}");
    }
}

// Редактирование
using (ApplicationContext db = new ApplicationContext())
{
    // получаем первый объект
    User? user = await db.Users.FirstOrDefaultAsync();
    if (user != null)
    {
        user.Name = "Bob";
        user.Age = 44;
        //обновляем объект
        await db.SaveChangesAsync();
    }
    // выводим данные после обновления
    Console.WriteLine("\nДанные после редактирования:");
    var users = await db.Users.ToListAsync();
    foreach (User u in users)
    {
        Console.WriteLine($"{u.Id}.{u.Name} - {u.Age}");
    }
}

// Удаление
using (ApplicationContext db = new ApplicationContext())
{
    // получаем первый объект
    User? user = await db.Users.FirstOrDefaultAsync();
    if (user != null)
    {
        //удаляем объект
        db.Users.Remove(user);
        await db.SaveChangesAsync();
    }
    // выводим данные после обновления
    Console.WriteLine("\nДанные после удаления:");
    var users = await db.Users.ToListAsync();
    foreach (User u in users)
    {
        Console.WriteLine($"{u.Id}.{u.Name} - {u.Age}");
    }
}