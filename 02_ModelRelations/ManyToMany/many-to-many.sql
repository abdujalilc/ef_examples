﻿CREATE TABLE "Courses" (
    "Id"    INTEGER NOT NULL,
    "Name"  TEXT,
    CONSTRAINT "PK_Courses" PRIMARY KEY("Id" AUTOINCREMENT)
);
 
CREATE TABLE "Students" (
    "Id"    INTEGER NOT NULL,
    "Name"  TEXT,
    CONSTRAINT "PK_Students" PRIMARY KEY("Id" AUTOINCREMENT)
);
 
CREATE TABLE "CourseStudent" (
    "CoursesId" INTEGER NOT NULL,
    "StudentsId"    INTEGER NOT NULL,
    CONSTRAINT "FK_CourseStudent_Courses_CoursesId" FOREIGN KEY("CoursesId") REFERENCES "Courses"("Id") ON DELETE CASCADE,
    CONSTRAINT "FK_CourseStudent_Students_StudentsId" FOREIGN KEY("StudentsId") REFERENCES "Students"("Id") ON DELETE CASCADE,
    CONSTRAINT "PK_CourseStudent" PRIMARY KEY("CoursesId","StudentsId")
);

------------------------------------------------

CREATE TABLE "Enrollments" (
    "StudentId" INTEGER NOT NULL,
    "CourseId"  INTEGER NOT NULL,
    "Mark"  INTEGER NOT NULL DEFAULT 3,
    CONSTRAINT "FK_Enrollments_Courses_CourseId" FOREIGN KEY("CourseId") REFERENCES "Courses"("Id") ON DELETE CASCADE,
    CONSTRAINT "FK_Enrollments_Students_StudentId" FOREIGN KEY("StudentId") REFERENCES "Students"("Id") ON DELETE CASCADE,
    CONSTRAINT "PK_Enrollments" PRIMARY KEY("CourseId","StudentId")
);
