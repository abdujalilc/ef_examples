﻿using Microsoft.EntityFrameworkCore;

public class ApplicationContext2 : DbContext
{
    public DbSet<Course2> Courses { get; set; } = null!;
    public DbSet<Student2> Students { get; set; } = null!;
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=helloapp.db");
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .Entity<Course2>()
            .HasMany(c => c.Students)
            .WithMany(s => s.Courses)
            .UsingEntity<Enrollment2>(
               j => j
                .HasOne(pt => pt.Student)
                .WithMany(t => t.Enrollments)
                .HasForeignKey(pt => pt.StudentId),
            j => j
                .HasOne(pt => pt.Course)
                .WithMany(p => p.Enrollments)
                .HasForeignKey(pt => pt.CourseId),
            j =>
            {
                j.Property(pt => pt.Mark).HasDefaultValue(3);
                j.HasKey(t => new { t.CourseId, t.StudentId });
                j.ToTable("Enrollments");
            });
    }
}

public class Course2
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public List<Student2> Students { get; set; } = new();
    public List<Enrollment2> Enrollments { get; set; } = new();

}
public class Student2
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public List<Course2> Courses { get; set; } = new();
    public List<Enrollment2> Enrollments { get; set; } = new();
}
public class Enrollment2
{
    public int StudentId { get; set; }
    public Student2? Student { get; set; }

    public int CourseId { get; set; }
    public Course2? Course { get; set; }

    public int Mark { get; set; }       // оценка студента
}
