﻿using Microsoft.EntityFrameworkCore;

class Test
{
    void test()
    {
        //add
        using (ApplicationContext2 db = new ApplicationContext2())
        {
            // пересоздадим базу данных
            db.Database.EnsureDeleted();
            db.Database.EnsureCreated();

            // создание и добавление моделей
            Student2 tom = new Student2 { Name = "Tom" };
            Student2 alice = new Student2 { Name = "Alice" };
            Student2 bob = new Student2 { Name = "Bob" };
            db.Students.AddRange(tom, alice, bob);

            Course2 algorithms = new Course2 { Name = "Алгоритмы" };
            Course2 basics = new Course2 { Name = "Основы программирования" };

            db.Courses.AddRange(algorithms, basics);

            // добавляем к студентам курсы
            tom.Enrollments.Add(new Enrollment2 { Course = algorithms });
            tom.Courses.Add(basics);
            alice.Enrollments.Add(new Enrollment2 { Course = algorithms, Mark = 4 });
            bob.Enrollments.Add(new Enrollment2 { Course = basics });

            db.SaveChanges();
        }
        //retrieve
        using (ApplicationContext2 db = new ApplicationContext2())
        {
            var courses = db.Courses.Include(c => c.Students).ToList();
            // выводим все курсы
            foreach (var c in courses)
            {
                Console.WriteLine($"Course: {c.Name}");
                // выводим всех студентов для данного кура
                foreach (Student2 s in c.Students)
                    Console.WriteLine($"Name: {s.Name}");
                Console.WriteLine("-------------------");
            }
            var courses2 = db.Courses.Include(c => c.Students).ToList();
            // выводим все курсы
            foreach (var c in courses2)
            {
                Console.WriteLine($"Course: {c.Name}");
                // выводим всех студентов для данного кура
                foreach (var s in c.Enrollments)
                    Console.WriteLine($"Name: {s.Student?.Name}  Mark: {s.Mark}");
                Console.WriteLine("-------------------");
            }
        }
        //edit 

        //delete
    }
}