﻿class Test
{
    void test()
    {
        using (ApplicationContext3 db = new ApplicationContext3())
        {
            User3 user1 = new User3("login1", "pass1234", new UserProfile3 { Age = 23, Name = "Tom" });
            User3 user2 = new User3("login2", "5678word2", new UserProfile3 { Age = 27, Name = "Alice" });
            db.Users.AddRange(user1, user2);
            db.SaveChanges();

            var users = db.Users.ToList();
            foreach (User3 user in users)
                Console.WriteLine(user.ToString());
        }
    }
}