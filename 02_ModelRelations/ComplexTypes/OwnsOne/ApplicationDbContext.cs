﻿using Microsoft.EntityFrameworkCore;

public class User3
{
    private User3() { }
    public User3(string login, string password, UserProfile3 profile)
    {
        Login = login; Password = password; Profile = profile;
    }
    public int Id { get; set; }
    public string? Login { get; set; }
    public string? Password { get; set; }
    private UserProfile3? Profile { get; set; }
    public override string ToString()
    {
        return $"Name: {Profile?.Name}  Age: {Profile?.Age}  Login: {Login} Password: {Password}";
    }
}
public class UserProfile3
{
    public string? Name { get; set; }
    public int Age { get; set; }
}
public class ApplicationContext3 : DbContext
{
    public DbSet<User3> Users { get; set; } = null!;
    public ApplicationContext3()
    {
        Database.EnsureDeleted();
        Database.EnsureCreated();
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=helloapp.db");
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User3>().OwnsOne(typeof(UserProfile3), "Profile");
    }
}