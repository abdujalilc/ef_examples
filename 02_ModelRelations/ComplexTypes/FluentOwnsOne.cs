﻿using Microsoft.EntityFrameworkCore;

public class User2
{
    public int Id { get; set; }
    public string? Login { get; set; }
    public string? Password { get; set; }
    public UserProfile2? Profile { get; set; }
}
public class UserProfile2
{
    public string? Name { get; set; }
    public int Age { get; set; }
}
public class ApplicationContext2 : DbContext
{
    public DbSet<User2> Users { get; set; } = null!;
    public ApplicationContext2()
    {
        Database.EnsureDeleted();
        Database.EnsureCreated();
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=helloapp.db");
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User2>().OwnsOne(u => u.Profile);
    }
}