﻿SELECT "u"."Id", "u"."CompanyId", "u"."Name", "c"."Id", "c"."Name"
FROM "Users" AS "u"
LEFT JOIN "Companies" AS "c" ON "u"."CompanyId" = "c"."Id"

---------------------------------------------
--thenInclude
SELECT "u"."Id", "u"."CompanyId", "u"."Name", "c"."Id", "c"."CountryId", "c"."Name", "c0"."Id", "c0"."Name"
FROM "Users" AS "u"
LEFT JOIN "Companies" AS "c" ON "u"."CompanyId" = "c"."Id"
LEFT JOIN "Countries" AS "c0" ON "c"."CountryId" = "c0"."Id"