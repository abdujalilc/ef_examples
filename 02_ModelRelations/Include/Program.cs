﻿using Microsoft.EntityFrameworkCore;

using (ApplicationContext db = new ApplicationContext())
{
    // пересоздадим базу данных
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();

    // добавляем начальные данные
    Company microsoft = new Company { Name = "Microsoft" };
    Company google = new Company { Name = "Google" };
    db.Companies.AddRange(microsoft, google);

    User tom = new User { Name = "Tom", Company = microsoft };
    User bob = new User { Name = "Bob", Company = google };
    User alice = new User { Name = "Alice", Company = microsoft };
    User kate = new User { Name = "Kate", Company = google };
    db.Users.AddRange(tom, bob, alice, kate);

    db.SaveChanges();

    // получаем пользователей
    var users = db.Users
        .Include(u => u.Company)  // подгружаем данные по компаниям
        .ToList();
    foreach (var user in users)
        Console.WriteLine($"{user.Name} - {user.Company?.Name}");
}
// метод Include не используется
using (ApplicationContext db = new ApplicationContext())
{
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();

    Company microsoft = new Company { Name = "Microsoft" };
    Company google = new Company { Name = "Google" };
    db.Companies.AddRange(microsoft, google);

    User tom = new User { Name = "Tom", Company = microsoft };
    User bob = new User { Name = "Bob", Company = google };
    User alice = new User { Name = "Alice", Company = microsoft };
    User kate = new User { Name = "Kate", Company = google };
    db.Users.AddRange(tom, bob, alice, kate);
    db.SaveChanges();

    var users = db.Users.ToList();
    foreach (var user in users)
        Console.WriteLine($"{user.Name} - {user.Company?.Name}");
}
//loaded already
using (ApplicationContext db = new ApplicationContext())
{
    var companies = db.Companies.ToList();
    // получаем пользователей
    var users = db.Users
        //.Include(u => u.Company)  // подгружаем данные по компаниям
        .ToList();
    foreach (var user in users)
        Console.WriteLine($"{user.Name} - {user.Company?.Name}");
}
using (ApplicationContext db = new ApplicationContext())
{
    var companies = db.Companies
                    .Include(c => c.Users)  // добавляем данные по пользователям
                    .ToList();
    foreach (var company in companies)
    {
        Console.WriteLine(company.Name);
        // выводим сотрудников компании
        foreach (var user in company.Users)
            Console.WriteLine(user.Name);
        Console.WriteLine("----------------------");     // для красоты
    }
}