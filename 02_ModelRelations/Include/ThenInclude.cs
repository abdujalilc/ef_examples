﻿using Microsoft.EntityFrameworkCore;

class Test
{
    void test()
    {
        using (ApplicationContext2 db = new ApplicationContext2())
        {
            // пересоздадим базу данных
            db.Database.EnsureDeleted();
            db.Database.EnsureCreated();

            Country usa = new Country { Name = "USA" };
            Country japan = new Country { Name = "Japan" };
            db.Countries.AddRange(usa, japan);

            // добавляем начальные данные
            Company1 microsoft = new Company1 { Name = "Microsoft", Country = usa };
            Company1 sony = new Company1 { Name = "Sony", Country = japan };
            db.Companies.AddRange(microsoft, sony);


            User1 tom = new User1 { Name = "Tom", Company = microsoft };
            User1 bob = new User1 { Name = "Bob", Company = sony };
            User1 alice = new User1 { Name = "Alice", Company = microsoft };
            User1 kate = new User1 { Name = "Kate", Company = sony };
            db.Users.AddRange(tom, bob, alice, kate);

            db.SaveChanges();
        }
        // получение данных
        using (ApplicationContext2 db = new ApplicationContext2())
        {
            // получаем пользователей
            var users = db.Users
                    .Include(u => u.Company)  // подгружаем данные по компаниям
                        .ThenInclude(c => c!.Country)    // к компаниям подгружаем данные по странам
                    .ToList();
            foreach (var user in users)
                Console.WriteLine($"{user.Name} - {user.Company?.Name} - {user.Company?.Country?.Name}");
        }
        //Chain
        using (ApplicationContext2 db = new ApplicationContext2())
        {
            var users = db.Users
                .Include(u => u.Company!.Country)
                .ToList();
            foreach (var user in users)
                Console.WriteLine($"{user.Name} - {user.Company?.Name} - {user.Company?.Country!.Name}");
        }
    }
}