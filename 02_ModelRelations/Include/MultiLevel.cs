﻿using Microsoft.EntityFrameworkCore;

public class ApplicationContext2 : DbContext
{
    public DbSet<User1> Users { get; set; } = null!;
    public DbSet<Company1> Companies { get; set; } = null!;
    public DbSet<Country> Countries { get; set; } = null!;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=helloapp.db");
    }
}
public class Country
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public List<Company1> Companies { get; set; } = new();
}
public class Company1
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public int CountryId { get; set; }
    public Country? Country { get; set; }
    public List<User1> Users { get; set; } = new();
}

public class User1
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public int? CompanyId { get; set; }
    public Company1? Company { get; set; }
}

