﻿public class Company2
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public List<User2> Users { get; set; } = new();
}

public class User2
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public int? CompanyId { get; set; }
    public Company2? Company { get; set; }
}
