﻿CREATE TABLE "Users" (
    "Id"    INTEGER NOT NULL,
    "Name"  TEXT,
    "CompanyId" INTEGER NOT NULL,
    CONSTRAINT "PK_Users" PRIMARY KEY("Id" AUTOINCREMENT),
    CONSTRAINT "FK_Users_Companies_CompanyId" FOREIGN KEY("CompanyId") REFERENCES "Companies"("Id") ON DELETE CASCADE
);

------------------------------

CREATE TABLE "Users" (
    "Id"    INTEGER NOT NULL,
    "Name"  TEXT,
    "CompanyId" INTEGER,
    CONSTRAINT "FK_Users_Companies_CompanyId" FOREIGN KEY("CompanyId") REFERENCES "Companies"("Id"),
    CONSTRAINT "PK_Users" PRIMARY KEY("Id" AUTOINCREMENT)
);