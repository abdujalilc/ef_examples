﻿using Microsoft.EntityFrameworkCore;

public class ApplicationContext2 : DbContext
{
    public DbSet<User> Users { get; set; } = null!;
    public DbSet<Company> Companies { get; set; } = null!;
    public ApplicationContext2()
    {
        Database.EnsureDeleted();
        Database.EnsureCreated();
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=helloapp.db");
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User3>()
            .HasOne(u => u.Company3)
            .WithMany(c => c.Users3)
            .OnDelete(DeleteBehavior.Cascade);
    }
}
public class Company3
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public List<User3> Users3 { get; set; } = new();
}

public class User3
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public Company3? Company3 { get; set; }
}