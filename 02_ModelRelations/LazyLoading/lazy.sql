﻿-- получение всех пользователей
SELECT "u"."Id", "u"."CompanyId", "u"."Name"
FROM "Users" AS "u"
// идет обращение к свойству Company, его компании нет в контексте
// поэтому выполняется sql-запрос
SELECT "c"."Id", "c"."Name"
FROM "Companies" AS "c"
WHERE "c"."Id" = @__p_0
--Tom - Microsoft
-- компания этого пользователя уже в контексте, не надо выполнять новый запрос 
--Alice - Microsoft
-- компании следующего пользователя нет в контексте
-- поэтому выполняется sql-запрос
SELECT "c"."Id", "c"."Name"
FROM "Companies" AS "c"
WHERE "c"."Id" = @__p_0
--Bob - Google
-- компания этого пользователя уже в контексте, не надо выполнять новый запрос
--Kate - Google