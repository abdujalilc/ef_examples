﻿using Microsoft.EntityFrameworkCore;

public class User1
{
    public int Id { get; set; }
    public string? Login { get; set; }
    public string? Password { get; set; }

    public UserProfile1? Profile { get; set; }
}

public class UserProfile1
{
    public int Id { get; set; }

    public string? Name { get; set; }
    public int Age { get; set; }

    public int UserKey { get; set; }
    public User1? User { get; set; }
}

public class ApplicationContext1 : DbContext
{
    public DbSet<User> Users { get; set; } = null!;
    public DbSet<UserProfile> UserProfiles { get; set; } = null!;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=helloapp.db");
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .Entity<User1>()
            .HasOne(u => u.Profile)
            .WithOne(p => p.User)
            .HasForeignKey<UserProfile1>(p => p.UserKey);
    }
}