﻿CREATE TABLE "UserProfiles" (
    "Id"    INTEGER NOT NULL,
    "Name"  TEXT,
    "Age"   INTEGER NOT NULL,
    "UserId"    INTEGER NOT NULL,
    CONSTRAINT "PK_UserProfiles" PRIMARY KEY("Id" AUTOINCREMENT),
    CONSTRAINT "FK_UserProfiles_Users_UserId" FOREIGN KEY("UserId") REFERENCES "Users"("Id") ON DELETE CASCADE
);

------------------------------


CREATE UNIQUE INDEX "IX_UserProfiles_UserId" ON "UserProfiles" ("UserId")

-------------------ToTable()-----------------
CREATE TABLE "Users" (
    "Id"    INTEGER NOT NULL,
    "Login" TEXT,
    "Password"  TEXT,
    "Name"  TEXT,
    "Age"   INTEGER,
    CONSTRAINT "PK_Users" PRIMARY KEY("Id" AUTOINCREMENT)
);