﻿using Microsoft.EntityFrameworkCore;

class Test
{
    void test()
    {
        using (ApplicationContext2 db = new ApplicationContext2())
        {
            // пересоздадим базу данных
            db.Database.EnsureDeleted();
            db.Database.EnsureCreated();

            User2 user1 = new User2 { Login = "login1", Password = "pass1234" };
            User2 user2 = new User2 { Login = "login2", Password = "5678word2" };
            db.Users.AddRange(user1, user2);

            UserProfile2 profile1 = new UserProfile2 { Age = 22, Name = "Tom", User = user1 };
            UserProfile2 profile2 = new UserProfile2 { Age = 27, Name = "Alice", User = user2 };
            db.UserProfiles.AddRange(profile1, profile2);

            db.SaveChanges();
        }
        using (ApplicationContext2 db = new ApplicationContext2())
        {
            // получим данные
            foreach (var u in db.Users.Include(u => u.Profile).ToList())
            {
                Console.WriteLine($"Name: {u.Profile?.Name} Age: {u.Profile?.Age}");
                Console.WriteLine($"Login: {u.Login}  Password: {u.Password} \n");
            }
        }
    }
}