﻿using Microsoft.EntityFrameworkCore;

public class User2
{
    public int Id { get; set; }
    public string? Login { get; set; }
    public string? Password { get; set; }

    public UserProfile2? Profile { get; set; }
}

public class UserProfile2
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public int Age { get; set; }
    public User2? User { get; set; }
}

public class ApplicationContext2 : DbContext
{
    public DbSet<User2> Users { get; set; } = null!;
    public DbSet<UserProfile2> UserProfiles { get; set; } = null!;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=helloapp.db");
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User2>()
            .HasOne(u => u.Profile).WithOne(p => p.User)
            .HasForeignKey<UserProfile2>(up => up.Id);
        modelBuilder.Entity<User2>().ToTable("Users");
        modelBuilder.Entity<UserProfile2>().ToTable("Users");
    }
}