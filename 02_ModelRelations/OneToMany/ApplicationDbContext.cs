﻿using Microsoft.EntityFrameworkCore;

public class Company
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public List<User> Users { get; set; } = new(); // сотрудники компании
}

public class User
{
    public int Id { get; set; }
    public string? Name { get; set; }

    public int CompanyId { get; set; }
    public Company? Company { get; set; }  // компания пользователя
}

public class ApplicationContext : DbContext
{
    public DbSet<Company> Companies { get; set; } = null!;
    public DbSet<User> Users { get; set; } = null!;
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=helloapp.db");
    }
}