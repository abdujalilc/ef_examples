﻿SELECT u.Id, u.CompanyId, u.Name, u.PositionId, c.Id, c.CountryId, c.Name, c0.Id, c0.CapitalId, c0.Name, 
c1.Id, c1.Name, p.Id, p.Name
FROM Users AS u
LEFT JOIN Companies AS c ON u.CompanyId = c.Id
LEFT JOIN Countries AS c0 ON c.CountryId = c0.Id
LEFT JOIN Cities AS c1 ON c0.CapitalId = c1.Id
LEFT JOIN Positions AS p ON u.PositionId = p.Id)

-----------------------------------