﻿public class Company
{
    public int Id { get; set; }
    public string? Name { get; set; } // название компании
    public List<User> Users { get; set; } = new();
}
public class User
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public int CompanyId { get; set; }      // внешний ключ
    public Company? Company { get; set; }    // навигационное свойство
}
/*
 CREATE TABLE "Users" (
    "Id"    INTEGER NOT NULL,
    "Name"  TEXT,
    "CompanyId" INTEGER NOT NULL,
    CONSTRAINT "FK_Users_Companies_CompanyId" FOREIGN KEY("CompanyId") REFERENCES "Companies"("Id") ON DELETE CASCADE,
    CONSTRAINT "PK_Users" PRIMARY KEY("Id" AUTOINCREMENT)
); 
CREATE TABLE "Companies" (
    "Id"    INTEGER NOT NULL,
    "Name"  TEXT,
    CONSTRAINT "PK_Companies" PRIMARY KEY("Id" AUTOINCREMENT)
);

-------------------------------------

public class User
{
    public int Id { get; set; }
    public string? Name { get; set; }
 
    public Company? Company { get; set; }   // навигационное свойство
}
--no cascade
CREATE TABLE "Users" (
    "Id"    INTEGER NOT NULL,
    "Name"  TEXT,
    "CompanyId" INTEGER,
    CONSTRAINT "PK_Users" PRIMARY KEY("Id" AUTOINCREMENT),
    CONSTRAINT "FK_Users_Companies_CompanyId" FOREIGN KEY("CompanyId") REFERENCES "Companies"("Id")
);
--no cascade if remove FK from child
CREATE TABLE "Users" (
    "Id"    INTEGER NOT NULL,
    "Name"  TEXT,
    "CompanyId" INTEGER,
    CONSTRAINT "FK_Users_Companies_CompanyId" FOREIGN KEY("CompanyId") REFERENCES "Companies"("Id"),
    CONSTRAINT "PK_Users" PRIMARY KEY("Id" AUTOINCREMENT)
);
 */