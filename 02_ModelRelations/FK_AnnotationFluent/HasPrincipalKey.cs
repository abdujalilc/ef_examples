﻿using Microsoft.EntityFrameworkCore;

public class ApplicationContext2 : DbContext
{
    public DbSet<User2> Users2 { get; set; } = null!;
    public DbSet<Company2> Companies2 { get; set; } = null!;
    public ApplicationContext2()
    {
        Database.EnsureDeleted();
        Database.EnsureCreated();
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=helloapp.db");
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User2>()
            .HasOne(u => u.Company)
            .WithMany(c => c.Users2)
            .HasForeignKey(u => u.CompanyName)
            .HasPrincipalKey(c => c.Name);
    }
}
public class Company2
{
    public int Id { get; set; }
    public string? Name { get; set; } // название компании

    public List<User2> Users2 { get; set; } = new();
}

public class User2
{
    public int Id { get; set; }
    public string? Name { get; set; }

    public string? CompanyName { get; set; }
    public Company2? Company { get; set; }
}
/*
 CREATE TABLE "Users" (
    "Id"    INTEGER NOT NULL,
    "Name"  TEXT,
    "CompanyName"   TEXT,
    CONSTRAINT "PK_Users" PRIMARY KEY("Id" AUTOINCREMENT),
    CONSTRAINT "FK_Users_Companies_CompanyName" FOREIGN KEY("CompanyName") REFERENCES "Companies"("Name")
);
 */