﻿using (ApplicationContext2 db = new ApplicationContext2())
{
    Company2 company1 = new Company2 { Name = "Google" };
    Company2 company2 = new Company2 { Name = "Microsoft" };
    User2 user1 = new User2 { Name = "Tom", Company = company1 };
    User2 user2 = new User2 { Name = "Bob", CompanyName = "Microsoft" };
    User2 user3 = new User2 { Name = "Sam", CompanyName = company2.Name };

    db.Companies2.AddRange(company1, company2);
    db.Users2.AddRange(user1, user2, user3);
    db.SaveChanges();

    foreach (var user in db.Users2.ToList())
    {
        Console.WriteLine($"{user.Name} работает в {user.Company?.Name}");
    }
}