﻿using System.ComponentModel.DataAnnotations.Schema;

public class Company
{
    public int Id { get; set; }
    public string? Name { get; set; } // название компании

    public List<User> Users { get; set; } = new();
}

public class User
{
    public int Id { get; set; }
    public string? Name { get; set; }

    public int? CompanyInfoKey { get; set; }
    [ForeignKey("CompanyInfoKey")]
    public Company? Company { get; set; }
}