﻿public class MenuItem
{
    public int Id { get; set; }
    public string? Title { get; set; }
    public int? ParentId { get; set; }
    public MenuItem? Parent { get; set; }
    public List<MenuItem> Children { get; set; } = new();
}
/*
 CREATE TABLE "MenuItems" (
    "Id"    INTEGER NOT NULL,
    "Title" TEXT,
    "ParentId"  INTEGER,
    CONSTRAINT "FK_MenuItems_MenuItems_ParentId" FOREIGN KEY("ParentId") REFERENCES "MenuItems"("Id"),
    CONSTRAINT "PK_MenuItems" PRIMARY KEY("Id" AUTOINCREMENT)
);
 */