﻿namespace LinqWebSamples.Models
{
    public class Group
    {
        public int GroupId { get; set; }
        public IEnumerable<int> Modules { get; set; }

        public static List<Group> GetGroups()
        {
            var groups = new List<Group>();
            groups.Add(new Group { GroupId = 1, Modules = new int[4] { 1, 2, 3, 4 } });
            groups.Add(new Group { GroupId = 2, Modules = new int[3] { 3, 4, 5 } });
            groups.Add(new Group { GroupId = 3, Modules = new int[3] { 5, 6, 7 } });
            groups.Add(new Group { GroupId = 4, Modules = new int[3] { 1, 2, 3 } });
            return groups;
        }
    }
}
