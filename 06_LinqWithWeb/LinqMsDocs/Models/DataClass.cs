public record Person(string FirstName, string LastName);
public record Pet(string Name, Person Owner);
public record Employee(string FirstName, string LastName, int EmployeeID);
public record Cat(string Name, Person Owner) : Pet(Name, Owner);
public record Dog(string Name, Person Owner) : Pet(Name, Owner);

public record Category(string Name, int ID);
public record Product(string Name, int CategoryID);