﻿using System.Collections.Generic;

namespace Domain.Interfaces
{
    public interface IPersonRepository : IGenericRepository<Person>
    {
        IEnumerable<Person> GetAdultPersons();

    }
}
