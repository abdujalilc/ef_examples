﻿using Domain;
using Domain.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace DataAccessEF.TypeRepository
{
    internal class PersonRepository : GenericRepository<Person>, IPersonRepository
    {
        public PersonRepository(PeopleContext context) : base(context)
        {

        }

        IEnumerable<Person> IPersonRepository.GetAdultPersons()
        {
            return context.Person.Where(pers => pers.Age >= 18).ToList();
        }
    }
}

