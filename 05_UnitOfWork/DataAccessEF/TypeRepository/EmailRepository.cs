﻿using Domain;
using Domain.Interfaces;

namespace DataAccessEF.TypeRepository
{
    internal class EmailRepository : GenericRepository<Email>, IEmailRepository
    {
        public EmailRepository(PeopleContext context) : base(context)
        {

        }
    }
}

