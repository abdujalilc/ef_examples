﻿using Domain;
using Domain.Interfaces;

namespace DataAccessEF.TypeRepository
{
    internal class AddressRepository : GenericRepository<Address>, IAdressRepository
    {
        public AddressRepository(PeopleContext context) : base(context)
        {

        }
    }
}

