# EF LINQ

This repository contains various examples and concepts related to Entity Framework (EF) and LINQ.

## Project Structure

- **01_Introduction/** - Basic introduction to LINQ.
- **02_ModelRelations/** - Working with entity relationships in Entity Framework.
- **03_Queries/** - LINQ query examples for fetching and manipulating data.
- **04_LINQ/WebPageLinq/** - Using LINQ in web applications.
- **05_UnitOfWork/** - Implementing the Unit of Work pattern with Entity Framework.
- **06_LinqWithWeb/** - Advanced LINQ usage in web development.

## Requirements

- .NET Core / .NET Framework
- Entity Framework Core
- Visual Studio

## Setup & Usage

1. Clone the repository:
   ```sh
   git clone https://gitlab.com/your-repo/ef_linq.git
   ```
2. Open `ef_linq.sln` in Visual Studio.
3. Navigate into the desired project directory:
   ```sh
   cd 03_Queries  # or any other directory
   ```
4. Run the application:
   ```sh
   dotnet run
   ```

## Contribution

Contributions are welcome! Feel free to fork the repository and submit a pull request.

## License

This project is licensed under the MIT License.
