﻿// ArrayList is defined in System.Collections
using System.Collections;

ArrayList classicList = new ArrayList();
classicList.AddRange(new int[] { 3, 4, 5 });
IEnumerable<int> sequence1 = classicList.Cast<int>();

DateTime offender = DateTime.Now;
classicList.Add(offender);

IEnumerable<int> sequence2 = classicList.OfType<int>(); // OK - Ignores offending DateTime

IEnumerable<int> sequence3 = classicList.Cast<int>(); // Throws exception

classicList.Clear();