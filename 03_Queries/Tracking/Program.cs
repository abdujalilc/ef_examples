﻿using Microsoft.EntityFrameworkCore;

Class2 class2 = new Class2();
class2.test();

using (ApplicationContext db = new ApplicationContext())
{
    // пересоздаем базу данных
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();

    User tom = new User { Name = "Tom", Age = 36 };
    User bob = new User { Name = "Bob", Age = 39 };
    User alice = new User { Name = "Alice", Age = 28 };
    User kate = new User { Name = "Kate", Age = 25 };

    db.Users.AddRange(tom, bob, alice, kate);
    db.SaveChanges();
}
using (ApplicationContext db = new ApplicationContext())
{
    var user = db.Users.FirstOrDefault();
    if (user != null)
    {
        user.Age = 18;
        db.SaveChanges();
    }

    var users = db.Users.ToList();
    foreach (var u in users)
        Console.WriteLine($"{u.Name} ({u.Age})");
}
//AsNoTracking
using (ApplicationContext db = new ApplicationContext())
{
    var user = db.Users.AsNoTracking().FirstOrDefault();
    if (user != null)
    {
        user.Age = 22;
        db.SaveChanges();
    }

    var users = db.Users.AsNoTracking().ToList();
    foreach (var u in users)
        Console.WriteLine($"{u.Name} ({u.Age})");
}
//for context level
using (ApplicationContext db = new ApplicationContext())
{
    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
    var user = db.Users.FirstOrDefault();
    if (user != null)
    {
        user.Age = 8;
        db.SaveChanges();
    }

    var users = db.Users.ToList();
    foreach (var u in users)
        Console.WriteLine($"{u.Name} ({u.Age})");
}
//know how many objects are tracked
using (ApplicationContext db = new ApplicationContext())
{
    var users = db.Users.ToList();

    foreach (var u in users)
        Console.WriteLine($"{u.Name} ({u.Age})");

    int count = db.ChangeTracker.Entries().Count();
    Console.WriteLine($"{count}");
}
//tracked
using (ApplicationContext db = new ApplicationContext())
{
    var user1 = db.Users.FirstOrDefault();
    var user2 = db.Users.FirstOrDefault();
    if (user1 != null && user2 != null)
    {
        Console.WriteLine($"Before User1: {user1.Name}   User2: {user2.Name}");

        user1.Name = "Bob";

        Console.WriteLine($"After User1: {user1.Name}   User2: {user2.Name}");
    }
}
//AsNoTracking
using (ApplicationContext db = new ApplicationContext())
{
    var user1 = db.Users.FirstOrDefault();
    var user2 = db.Users.AsNoTracking().FirstOrDefault();

    if (user1 != null && user2 != null)
    {
        Console.WriteLine($"Before User1: {user1.Name}   User2: {user2.Name}");

        user1.Name = "Bob";

        Console.WriteLine($"After User1: {user1.Name}   User2: {user2.Name}");
    }
}