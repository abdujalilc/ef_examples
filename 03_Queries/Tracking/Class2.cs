﻿using Microsoft.EntityFrameworkCore.ChangeTracking;

public class Class2
{
    public void test()
    {
        using (var ctx = new ApplicationContext())
        {

            Console.WriteLine("Find Student");
            var std1 = ctx.Users.Find(1);

            Console.WriteLine("Context tracking changes of {0} entity.", ctx.ChangeTracker.Entries().Count());

            DisplayTrackedEntities(ctx.ChangeTracker);

            Console.WriteLine("Find Standard");

        }
    }

    private static void DisplayTrackedEntities(ChangeTracker changeTracker)
    {
        Console.WriteLine("");

        var entries = changeTracker.Entries();
        foreach (var entry in entries)
        {
            Console.WriteLine("Entity Name: {0}", entry.Entity.GetType().FullName);
            Console.WriteLine("Status: {0}", entry.State);
        }
        Console.WriteLine("");
        Console.WriteLine("---------------------------------------");
    }
}
