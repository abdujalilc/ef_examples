﻿using WebPageQueries.Models;

namespace WebPageQueries.Contexts
{
    public class DataInitializer
    {
        public static async Task SeedData(ApplicationContext _context)
        {
            if (!_context.Companies.Any() && !_context.Users.Any())
            {
                _context.Database.EnsureDeleted();
                _context.Database.EnsureCreated();

                Role adminRole = new Role { Name = "admin" };
                Role userRole = new Role { Name = "user" };
                _context.Roles.AddRange(userRole, adminRole);

                Country usa = new Country { Name = "USA" };

                Company microsoft = new Company { Name = "Microsoft", Country = usa };
                Company google = new Company { Name = "Google", Country = usa };
                _context.Companies.AddRange(microsoft, google);

                User tom = new User { Name = "Tom", Age = 36, Company = microsoft, Role = userRole };
                User bob = new User { Name = "Bob", Age = 39, Company = google, Role = userRole };
                User alice = new User { Name = "Alice", Age = 28, Company = microsoft, Role = adminRole };
                User kate = new User { Name = "Kate", Age = 25, Company = google, Role = adminRole };

                _context.Users.AddRange(tom, bob, alice, kate);
                _context.SaveChanges();
            }
        }
    }
}
