﻿using Microsoft.EntityFrameworkCore;
using WebPageQueries.Models;

namespace WebPageQueries.Contexts
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; } = null!;
        public DbSet<Company> Companies { get; set; } = null!;
        public DbSet<Country> Countries { get; set; } = null!;
        public DbSet<Role> Roles { get; set; } = null!;
        
    }
}
