﻿namespace WebPageQueries.ViewModels
{
    public class PetOwner
    {
        public string Name { get; set; }
        public List<String> Pets { get; set; }
    }
}
