using WebPageQueries.Contexts;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
builder.Services.AddRazorPages().AddRazorRuntimeCompilation();
string? con_string = "Data Source=AppData\\LinqQueries.db";
builder.Services.AddSqlite<ApplicationContext>(con_string);
WebApplication app = builder.Build();

using var serviceScope = app.Services.GetRequiredService<IServiceScopeFactory>().CreateScope();
ApplicationContext applicationDbContext = serviceScope.ServiceProvider.GetRequiredService<ApplicationContext>();
DataInitializer.SeedData(applicationDbContext).Wait();

app.UseStaticFiles();
app.UseRouting();
app.MapRazorPages();
app.MapGet("/debug/routes", (IEnumerable<EndpointDataSource> endpointSources) =>
        string.Join("\n", endpointSources.SelectMany(source => source.Endpoints)));
app.Run();